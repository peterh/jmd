## JMD - Jordan Meteorological Department
### Objective classification and identification of recurring large-scale circulation patterns

_Peter Hoffmann_
<br>
_Potsdam Institute of Climate Impact Research_
<br>
_Climate Resilience / Hydro-Climatic Risks_
<br>
_Captain Rain_


### Summary

Atmospheric fields of the geopotential height at 500 Pa (Z500) are often used by meteorologists to characterize the  large-scale circulation conditions and transport processes of air masses across geographical regions. Their features are recurring and often associated with characteristic local weather situations. A retrospective classification of days with similar characteristics allows experts a more comprehensive assessment of e.g. historical extreme weather events and predicted developments. 
The provided time series include sequences of recurring features of 20 different and automatic detected synoptic condition for the Eastern Mediterranean domain. The used appoach is based on an established image recognition technique (Structural Similarity). By combining this weather-type sequences with local meteorolgical station data they can be filtered by the attributes to identify the large-scale context for extreme weather events (e.g. extreme rainfall).

#### Classification

```mermaid

%%{init: {'theme': 'neutral', "flowchart" : { "curve" : "basis" } } }%%

graph LR;

A(Z500<sub> d</sub><hr>Atmosphere Fields) == 1951-1980 ==> B(SSIM <sub>d,d</sub><hr>Correlation
Matrix);
B ==> C(WT <sub>k,d</sub><hr>Hierarchical Clustering);
C ==> E(date;cluster<br>1951-01-01;WT01<br>1980-12-31;WT08);

```

**Atmospheric Fields:**<br>
ERA5 daily reanalysis fields of the geopotential height at 500 hPa (Z500) are used as input data to objectively identify a catalogue of recurring large-scale weather types (WT). It represents the circulation conditions in the middle troposphere at ca.5 km altitude. The data can be downloaded via [Climate Explorer](https://climexp.knmi.nl/selectdailyfield2.cgi?id=someone@somewhere) back to 1950 to date with a spatial resolution of 0.5° x 0.5°.  For clipping the domain of interest (EU[^2], EM[^3], CA[^4]), splitting and interpolating the data to a 1.0 ° x 1.0° we apply the CDO commands given below.

[^2]: North-Atlantic/Europe
[^3]: Eastern Mediterranean
[^4]: Central Asia 

**Structural Similarity:**<br>
For quantifying the similarity between day-to-day atmospheric fields a structural similarity index (SSIM[^1]) is applied to determine a correlation matrix. Every day is correlated to each other. The higher the SSIM the higher the similarity. The time period from 1951 to 1980 is used for training and identifying recurring circulation patterns. This step costs most of the computation time.  

**Clustering:**<br>
Finally a hierachical clustering is applied to the similarity matrix by prescribing the number of clusters (N=20). For reasons of understanding the number of days per cluster are counted and ranked. The numbering of the clusters starts with the most dominant one (WT01) and ends with WT20. The respective large-scale circulations patterns (weather-types) are given below: 

North-Atlantic/Europe|Eastern Mediterranean
---|---
![](data/era5_z500_daily_1951-1980_1x1_EU_20.png){height=200px}|![](data/era5_z500_daily_1951-1980_1x1_EM_20.png){height=200px}
**WT01:** Westerly Cyclonic|
**WT07:** South-West Cyclonic|
**WT08:** High over British Island|
**WT12:** Trough Central Europe|
**WT19:** Low over Central Europe|

[^1]: Zhou Wang, A. C. Bovik, H. R. Sheikh and E. P. Simoncelli, "Image quality assessment: from error visibility to structural similarity," in IEEE Transactions on Image Processing, vol. 13, no. 4, pp. 600-612, April 2004, doi: 10.1109/TIP.2003.819861. keywords: {Image quality;Humans;Transform coding;Visual system;Visual perception;Data mining;Layout;Quality assessment;Degradation;Indexes}, 

#### Identification

```mermaid

%%{init: {'theme': 'neutral', "flowchart" : { "curve" : "basis" } } }%%

graph LR;

A(Z500<sub> x,y,d</sub><hr>Atmospheric Fields) == 1951-1980 ==> C((Decision Tree<hr>Training));
B(WT<sub> k,d</sub><hr>Weather-Types) == 1951-1980 ==> C;
C ==> D((Decision Tree<hr>Predicting));
E(Z500<sub> x,y,t</sub><hr>Atmospheric Fields) == 1981-2023 ==> D;
D ==> F(WT<sub> k,t</sub><hr>Weather-Types);
  
``` 

**Decision Tree:**<br>
The time series of weather-type sequences is provided in a csv table format. An example is given below. The number of patterns in the column _weather-types_ ranges from _WT01_ to _WT20_. The respective shapes of the large-scale circulation for the Eastern Mediterranean are illustrated in the _Fig.1_ below.

```mermaid

%%{init: {'theme': 'neutral', "flowchart" : { "curve" : "basis" } } }%%

graph TB

A[1
⬛️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
] --> D(<font size=5><b>WT01</b></font>
WT01 ... WT20
)
B[2
⬜️ ⬛️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
] --> D
C[N
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬛️
] --> D

```

#### Characterization

```mermaid

%%{init: {'theme': 'neutral', "flowchart" : { "curve" : "basis" } } }%%

graph LR;
A(WT<sub> k,d</sub><hr>Weather-Types) == 1981-2023 ==> C(LWTC <sub> k,m</sub><hr>Local WT Character);
B(LW<sub> d</sub><hr>Local Weather) == 1981-2023 ==> C;
  
```

#### Application

**Case Studies:**<br>

| Context | Country | Short |
| --- | --- | --- |
| Europe | Germany | EU_DEU |
| Eastern Mediterrean | Jordan | EM_JOR |
| Central Asia | Pakistan | CA_PAK |

### CDO Commands:

**Target Grid:**<br>

```
gridtype = lonlat
xsize    = 180
ysize    = 90
xfirst   = -180.0
xinc     = 1
yfirst   = -90.0
yinc     = 1
```

**Clipping:**<br>

```
cdo -sellonlatbox,10,60,20,45 -remapbil,targetgrid -selyear,1951/1980 era5_z500_daily_1950-2023.nc era5_z500_daily_1951-1980_1x1_JO_train.cdf
cdo -sellonlatbox,10,60,20,45 -remapbil,targetgrid -selyear,1981/2023 era5_z500_daily_1950-2023.nc era5_z500_daily_1981-2023_1x1_JO_pred.cdf

```

**Data Format:**<br>

```
year;month;day;weather-type
1981;1;1;WT01
1981;1;2;WT01
1981;1;3;WT01
1981;1;4;WT01
1981;1;5;WT01
1981;1;6;WT01
1981;1;7;WT01
1981;1;8;WT03
1981;1;9;WT03