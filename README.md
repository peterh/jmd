## JMD - Jordan Meteorological Department
### Objective classification and identification of recurring large-scale circulation patterns

_Peter Hoffmann_
<br>
_Potsdam Institute of Climate Impact Research_
<br>
_Climate Resilience / Hydro-Climatic Risks_
<br>
_Captain Rain_


### Summary

Atmospheric fields of the geopotential height at 500 Pa (Z500) are often used by meteorologists to characterize the large-scale circulation conditions and transport processes of air masses across geographical regions. Their features are recurring and often associated with characteristic local weather situations and phenomena. A retrospective classification of days with similar characteristics and shapes of the iso-contours allows experts a more comprehensive assessment of the large-scale context of e.g. historical extreme weather events and predicted developments. The methods are used based on image recognition technigues (structural simularity), hierarchical clustering and random forest ML approach. The latter enables a fast re-identification of weather-types in weather- and climate forecasts.

#### Typograms



![](figs/context.png){height=100px}

![](figs/typ_WZ.png){height=120px}|![](figs/typ_TRM.png){height=120px}|![](figs/typ_HM.png){height=120px}
---|---|---
![](figs/typ_JET.png){height=120px}|![](figs/typ_OMEGA.png){height=120px}|![](figs/typ_TM.png){height=120px}

#### Classification

```mermaid

%%{init: {'theme': 'neutral', "flowchart" : { "curve" : "basis" } } }%%

graph LR;

    A(<b>Z500<sub> d</sub></b><hr>Atmosphere Fields) -->|1951-1980| B(<b>SSIM <sub>d,d</sub></b><hr>Correlation Matrix);
    B --> C(<b>WT <sub>k,d</sub></b><hr>Hierarchical Clustering);
    C --> E(<b>date,cluster</b><br>1951-01-01,WT01<br>1980-12-31,WT08);

```

**Atmospheric Fields:**<br>
ERA5 daily reanalysis fields of the geopotential height at 500 hPa (Z500) are used as input data to objectively identify a catalogue of recurring large-scale weather types (WT). It represents the circulation conditions in the middle troposphere at ca.5 km altitude. The data can be downloaded via [Climate Explorer](https://climexp.knmi.nl/selectdailyfield2.cgi?id=someone@somewhere) back to 1950 to date with a spatial resolution of 0.5° x 0.5°.  For clipping the domain of interest (EU[^2], EM[^3], CA[^4]), splitting and interpolating the data to a 1.0 ° x 1.0° we apply the CDO commands given below.

[^2]: North-Atlantic/Europe
[^3]: Eastern Mediterranean
[^4]: Central Asia 

**Structural Similarity:**<br>
For quantifying the similarity between day-to-day atmospheric fields a structural similarity index (SSIM[^1]) is applied to determine a correlation matrix. Every day is correlated to each other. The higher the SSIM the higher the similarity. The time period from 1951 to 1980 is used for training and identifying recurring circulation patterns. This step costs most of the computation time.  

**Clustering:**<br>
Finally a hierachical clustering is applied to the similarity matrix by prescribing the number of clusters (N=20). For reasons of understanding the number of days per cluster are counted and ranked. The numbering of the clusters starts with the most dominant one (WT01) and ends with WT20. The respective large-scale circulations patterns (weather-types) are given below: 

North-Atlantic/Europe|Eastern Mediterranean
---|---
![](data/era5_z500_daily_1951-1980_1x1_EU_20.png){height=200px}|![](data/era5_z500_daily_1951-1980_1x1_EM_20.png){height=200px}
**WT01:** Westerly Cyclonic|**WT01:** Westerly Winds
**WT07:** South-West Cyclonic|**WT05:** Trough over Greece
**WT08:** High over British Island|**WT07:** Trough over Turkey
**WT12:** Trough Central Europe|
**WT19:** Low over Central Europe|

[^1]: Zhou Wang, A. C. Bovik, H. R. Sheikh and E. P. Simoncelli, "Image quality assessment: from error visibility to structural similarity," in IEEE Transactions on Image Processing, vol. 13, no. 4, pp. 600-612, April 2004, doi: 10.1109/TIP.2003.819861. keywords: {Image quality;Humans;Transform coding;Visual system;Visual perception;Data mining;Layout;Quality assessment;Degradation;Indexes}, 

#### Identification

```mermaid

%%{init: {'theme': 'neutral', "flowchart" : { "curve" : "basis" } } }%%

graph LR;

A(<b>Z500<sub> x,y,d</sub></b><hr>Atmospheric Fields) -->|1951-1980| C((<b>Decision Tree</b><hr>Training));
B(<b>WT<sub> k,d</sub></b><hr>Weather-Types) -->|1951-1980| C;
C --> D((<b>Decision Tree</b><hr>Predicting));
E(<b>Z500<sub> x,y,t</sub></b><hr>Atmospheric Fields) --> |1981-2023| D;
D --> F(<b>WT<sub> k,t</sub></b><hr>Weather-Types);
  
```

**Decision Tree:**<br>
For the re-identification of existing weather-types (WT) in predicted/simulated atmospheric fields of Z500 we train a decision tree (supervised learning). For the training period 1951-1980 the weather-type sequences are defined as target values (features) and the Z500 time series at each grid cell as independent values representing the atmospheric fields (data). The derived rules are used to classify new atmosphere data fields.



```mermaid

%%{init: {'theme': 'neutral', "flowchart" : { "curve" : "basis" } } }%%

graph LR;

subgraph Training;
subgraph Day 1

A0(1
⬛️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
) --> D0(<b><font size=5>WT02</font></b><hr>WT01 - WT20)

B0(2
⬜️ ⬛️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
) --> D0;

C0(N
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬛️
) --> D0;

end

subgraph Day 2

A1(1
⬛️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
) --> D1(<b><font size=5>WT05</font></b><hr>WT01 - WT20)

B1(2
⬜️ ⬛️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
) --> D1;

C1(N
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬛️
) --> D1;

end

subgraph Day N

A2(1
⬛️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
) --> D2(<b><font size=5>WT12</font></b><hr>WT01 - WT20)

B2(2
⬜️ ⬛️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
) --> D2;

C2(N
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️
⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬜️ ⬛️
) --> D2;

end

end;

```

#### Characterization

For the assessment and interpretation of the weather-types (WT) we calculated long-term monthly mean maps (N=240) of temperature and precipitation on the country level. It can be easily distinguish between wet, dry, warm and cold weather types in the course of a year..


```mermaid

%%{init: {'theme': 'neutral', "flowchart" : { "curve" : "basis" } } }%%

graph LR;
A(<b>WT<sub> k,d</sub></b><hr>Weather-Types) -->|1981-2023| C(<b>LWTC <sub> k,m</sub></b><hr>Local WT Character);
B(<b>LW<sub> d</sub></b><hr>Local Weather) -->|1981-2023| C;
  
```

Temperature Anomalies|Total Precipitation
---|---
![](data/DEU_ano.png)|![](data/DEU_tp.png)
warm/cold weather-types|dry/wet weather-types

**Icons:**

sunny|windy|hot|icy|rainy|flood
---|---|---|---|---|---
![](figs/sunny.png){height=50px}|![](figs/windy.png){height=50px}|![](figs/hot.png){height=50px}|![](figs/icy.png){height=50px}|![](figs/rainy.png){height=50px}|![](figs/flood.png){height=50px}
snow|cloudy|bright|stormy|thunderstorm|icons
![](figs/snow.png){height=50px}|![](figs/cloudy.png){height=50px}|![](figs/bright.png){height=50px}|![](figs/stormy.png){height=50px}|![](figs/thunderstorm.png){height=50px}|![](figs/icon.png){height=50px}


#### Application

**Case Studies:**<br>

| Context | Country | Short |
| --- | --- | --- |
| Europe | Germany | EU_DEU |
| Eastern Mediterrean | Jordan | EM_JOR |
| Central Asia | Pakistan | CA_PAK |
| **Europe** | **Danube, Elbe, Rhine** | **CMIP6** |

**GFS Forecast:**

Latest|Chart
---|---
![](latest.png)|![](jmd_JOR.png)
allocated atmospheric fields to weather-types|weather-types in forecasts

**Ensemble**

|SEQ|-14|-13|-12|-11|-10|-09|-08|-07|-06|-05|-04|-03|-02|-01|NOW|+01|+02|+03|+04|+05|+06|+07|+08|+09|+10|+11|+12|+13|+14|
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
|-14| BM| BM| HB| HB| BM| BM|TRW|TRM|TRM|TRM|TRM|TRM|  W|  W| SW|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
|-13|---| BM| HB| HB| BM|  W|  W|  W|  W|  W|  W|  W|  W|  W|  W|  W|---|---|---|---|---|---|---|---|---|---|---|---|---|
|-12|---|---| HB| HB| BM|  W|  W|TRM|TRM|TRM|  W|  W|  W| SW| SW|  W| SW|---|---|---|---|---|---|---|---|---|---|---|---|
|-11|---|---|---| HB| BM|  W|  W|  W|  W|  W|  W|  W|  W|  W|  W|  W|  W| SW|---|---|---|---|---|---|---|---|---|---|---|
|-10|---|---|---|---| BM|  W| NW|  W|  W|  W|  W|  W|  W| SW| SW| SW|  W|  W|  W|---|---|---|---|---|---|---|---|---|---|
|-09|---|---|---|---|---| BM| NW|  W|  W|  W|  W|  W|  W| SW| SW| SW| SW| SW| SW|  S|---|---|---|---|---|---|---|---|---|
|-08|---|---|---|---|---|---| NW|  W|  W|TRM|  W|  W|  W|  W|  W|  W|  W|  W|  W|  W|  N|---|---|---|---|---|---|---|---|
|-07|---|---|---|---|---|---|---|  W|  W|  W|  W|  W|  W|  W|  W| HN| HN| SE| HM| HM| BM| BM|---|---|---|---|---|---|---|
|-06|---|---|---|---|---|---|---|---|  W|  W|  W|  W|  W| SW| SW| BM| SE| SE| SE| SE| BM| BM| BM|---|---|---|---|---|---|
|-05|---|---|---|---|---|---|---|---|---|  W|  W|  W|  W| SW| SW|  W|  W| BM| HF| HF| HF| HF| BM|TRW|---|---|---|---|---|
|-04|---|---|---|---|---|---|---|---|---|---|  W|  W|  W| SW| SW|  W|  W| HM| HM| HM|  W| SW| SW|  W|  W|---|---|---|---|
|-03|---|---|---|---|---|---|---|---|---|---|---|  W|  W| SW| SW|  W|  W|  W| HM| BM| HM| HM|  S| SW|  W|  W|---|---|---|
|-02|---|---|---|---|---|---|---|---|---|---|---|---|  W| SW| SW|  W|  W| BM| SW| SW| SW|  W| HB| HB| HB| HB| BM|---|---|
|-01|---|---|---|---|---|---|---|---|---|---|---|---|---| SW| SW|  W|  W| BM| HM| HM| BM| BM|  W|  W|  W| HM| HM| HM|---|
|NOW|---|---|---|---|---|---|---|---|---|---|---|---|---|---| SW|  W|  W| HB| BM| BM| SE| SE|  W|  W|  W| SW|  W|  W|  W|
|**SEQ**|**BM**|**BM**|**HB**|**HB**|**BM**|**W**|**W**|**W**|**W**|**W**|**W**|**W**|**W**|**SW**|**SW**|**W**|**W**|**BM**|**HM**|**HM**|**BM**|**BM**|**BM**|**W**|**W**|**W**|**W**|**W**|**W**|
|**ICO**|🌤️|🌤️|☀️|☀️|🌤️|🌬️|🌬️|🌬️|🌬️|🌬️|🌬️|🌬️|🌬️|🌡️|🌡️|🌬️|🌬️

**Demo:**

![](figs/demo.gif){height=400px}

**Data:**

```mermaid
%%{init: {'theme': 'forest', "flowchart" : { "curve" : "basis" } } }%%
gantt
    title Time Horizon
    dateFormat X
    axisFormat %s
    section Training
    ERA5: 1951,1980
    section Testing
    ERA5: active,1981,2023
    section Predicting
    GFS/CFS: done,2023,2024
    CMIP6: done,1981,2100
```

## Historical Event: (Flood Elbe)

```mermaid
graph LR
A(May 21<br><b>TRM</b><hr><font size=5>⛅️</font>) --> B
B(May 22<br><b>TRM</b><hr><font size=5>⛈️</font>) --> C
C(May 23<br><b>TRM</b><hr><font size=5>🌧️</font>) --> D
D(May 24<br><b>TM</b><hr><font size=5>🌧️</font>) --> E
E(May 25<br><b>TM</b><hr><font size=5>🌧️</font>) --> F
F(May 26<br><b>TM</b><hr><font size=5>🌧️</font>) --> G
G(May 27<br><b>HF</b><hr><font size=5>⛅️</font>) --> H
H(May 28<br><b>TM</b><hr><font size=5>🌧️</font>) --> I
I(May 29<br><b>TM</b><hr><font size=5>🌧️</font>) --> J
J(May 30<br><b>NE</b><hr><font size=5>💧</font>) --> K
K(May 31<br><b>NE</b><hr><font size=5>💧</font>)
classDef wt fill:white
class A,B,C,D,E,F,G,H,I,J,K wt
```

### CDO Commands:

**Target Grid:**<br>

```
gridtype = lonlat
xsize    = 180
ysize    = 90
xfirst   = -180.0
xinc     = 1
yfirst   = -90.0
yinc     = 1
```

**Clipping:**<br>

```
cdo -sellonlatbox,10,60,20,45 -remapbil,targetgrid -selyear,1951/1980 era5_z500_daily_1950-2023.nc era5_z500_daily_1951-1980_1x1_JO_train.cdf
cdo -sellonlatbox,10,60,20,45 -remapbil,targetgrid -selyear,1981/2023 era5_z500_daily_1950-2023.nc era5_z500_daily_1981-2023_1x1_JO_pred.cdf
```

**Data Format:**<br>

```x^x^
year;month;day;wt
1981;1;1;WT01
1981;1;2;WT01
1981;1;3;WT01
1981;1;4;WT01
1981;1;5;WT01
1981;1;6;WT01
1981;1;7;WT01
1981;1;8;WT03
1981;1;9;WT03
```
